package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Nina", "Merchant");
    }

    @Test
    public void testMethodGetName() {
        //TODO: Complete me
        assertEquals(member.getName(), "Nina");
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals(member.getRole(), "Merchant");
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        //TODO: Complete me
        Member newmem = new OrdinaryMember("Budi", "Fisherman");
        member.addChildMember(newmem);
        assertEquals(member.getChildMembers().size(), 0);
        member.removeChildMember(newmem);
        assertEquals(member.getChildMembers().size(), 0);
    }
}
