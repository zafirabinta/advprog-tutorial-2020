package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals(member.getName(), "Wati");
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals(member.getRole(), "Gold Merchant");
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        member.addChildMember(new OrdinaryMember("Budi", "Fisherman"));
        assertEquals(member.getChildMembers().size(), 1);
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member newmem = new OrdinaryMember("Budi", "Fisherman");
        member.addChildMember(newmem);
        member.removeChildMember(newmem);
        assertEquals(member.getChildMembers().size(), 0);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        member.addChildMember(new OrdinaryMember("Budi", "Fisherman"));
        member.addChildMember(new OrdinaryMember("Budi", "Fisherman"));
        member.addChildMember(new OrdinaryMember("Budi", "Fisherman"));
        member.addChildMember(new OrdinaryMember("Budi", "Fisherman"));
        assertEquals(member.getChildMembers().size(), 3);
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member guildMaster = new PremiumMember("Tono", "Master");
        guildMaster.addChildMember(new OrdinaryMember("Budi", "Fisherman"));
        guildMaster.addChildMember(new OrdinaryMember("Budi", "Fisherman"));
        guildMaster.addChildMember(new OrdinaryMember("Budi", "Fisherman"));
        guildMaster.addChildMember(new OrdinaryMember("Budi", "Fisherman"));
        assertEquals(guildMaster.getChildMembers().size(), 4);
    }
}
