package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;
import java.util.*;

public class PremiumMember implements Member {
    private String name;
    private String role;
    private ArrayList<Member> memberBawahan = new ArrayList<Member>();

    public PremiumMember(String name, String role){
        this.name = name;
        this.role = role;
    }
    @java.lang.Override
    public String getName() {

        return this.name;
    }

    @java.lang.Override
    public String getRole() {

        return this.role;
    }

    @java.lang.Override
    public void addChildMember(Member member) {
        if(memberBawahan.size() < 3 || this.role.equals("Master")){
            memberBawahan.add(member);
        }
    }

    @java.lang.Override
    public void removeChildMember(Member member) {
        memberBawahan.remove(member);
    }

    @java.lang.Override
    public List<Member> getChildMembers() {

        return this.memberBawahan;
    }
    //TODO: Complete me

}
