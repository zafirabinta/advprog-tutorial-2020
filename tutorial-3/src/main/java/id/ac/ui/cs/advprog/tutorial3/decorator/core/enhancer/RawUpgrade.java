package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.Randomizer;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;
    int randomVal;

    public RawUpgrade(Weapon weapon) {

        this.weapon= weapon;
        this.randomVal = Randomizer.randInt(5, 10);
    }

    @Override
    public String getName() {

        return "Raw " + weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        if(weapon != null){
            return weapon.getWeaponValue() + randomVal;
        }
        else {
            return randomVal;
        }
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Raw " + weapon.getDescription();
    }
}
