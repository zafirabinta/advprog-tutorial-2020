package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.Randomizer;
import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;
    int randomVal;

    public ChaosUpgrade(Weapon weapon) {

        this.weapon= weapon;
        this.randomVal = Randomizer.randInt(50,55);
    }

    @Override
    public String getName() {
        return "Chaos " + weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        if(weapon != null){
            return weapon.getWeaponValue() + randomVal;
        }
        else{
            return randomVal;
        }
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Chaos " + weapon.getDescription();
    }
}
