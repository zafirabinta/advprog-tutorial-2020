package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.Randomizer;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;
    int randomVal;

    public MagicUpgrade(Weapon weapon) {

        this.weapon= weapon;
        randomVal = Randomizer.randInt(15, 20);
    }

    @Override
    public String getName() {

        return "Magic " + weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        if(weapon != null){
            return weapon.getWeaponValue() + randomVal;
        }
        else {
            return randomVal;
        }
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Magic " + weapon.getDescription();
    }
}
