package id.ac.ui.cs.advprog.tutorial3.decorator.repository;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer.EnhancerDecorator;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class EnhanceRepository {

    public void enhanceToAllWeapons(ArrayList<Weapon> weapons){
        Weapon weaponEnhance;
        int index;
        for (Weapon weapon: weapons) {
            switch (weapon.getName()) {
                case "Gun":
                    weaponEnhance = WeaponProducer.WEAPON_GUN.createWeaponEnhancer();
                    weaponEnhance = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weaponEnhance);
                    weaponEnhance = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weaponEnhance);
                    index = weapons.indexOf(weapon);
                    weapons.set(index,weaponEnhance);
                    break;
                case "Longbow":
                    weaponEnhance = WeaponProducer.WEAPON_LONGBOW.createWeaponEnhancer();
                    weaponEnhance = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weaponEnhance);
                    weaponEnhance = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weaponEnhance);
                    weaponEnhance = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weaponEnhance);
                    index = weapons.indexOf(weapon);
                    weapons.set(index,weaponEnhance);
                    break;

                case "Shield":
                    weaponEnhance = WeaponProducer.WEAPON_SHIELD.createWeaponEnhancer();
                    weaponEnhance = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weaponEnhance);
                    weaponEnhance = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weaponEnhance);
                    index = weapons.indexOf(weapon);
                    weapons.set(index,weaponEnhance);
                    break;

                case "Sword":
                    weaponEnhance = WeaponProducer.WEAPON_SWORD.createWeaponEnhancer();
                    weaponEnhance = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weaponEnhance);
                    weaponEnhance = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weaponEnhance);
                    index = weapons.indexOf(weapon);
                    weapons.set(index,weaponEnhance);
                    break;

            }
        }
    }
}
