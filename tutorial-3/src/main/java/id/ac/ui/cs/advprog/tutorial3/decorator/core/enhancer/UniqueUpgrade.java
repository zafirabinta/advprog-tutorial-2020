package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.Randomizer;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;
    int randomVal;

    public UniqueUpgrade(Weapon weapon){

        this.weapon = weapon;
        this.randomVal = Randomizer.randInt(10, 15);
    }

    @Override
    public String getName() {

        return "Unique " + weapon.getName();
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        if(weapon != null){
            return weapon.getWeaponValue() + randomVal;
        }
        else {
            return randomVal;
        }
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Unique " + weapon.getDescription();
    }
}
