package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
    private String name;
    private String role;
    private ArrayList<Member> memberBawahan = new ArrayList<Member>();

    public OrdinaryMember(String name, String role){
        this.name = name;
        this.role = role;
    }
    public String getName() {

        return this.name;
    }

    @java.lang.Override
    public String getRole() {

        return this.role;
    }

    @java.lang.Override
    public void addChildMember(Member member) {

    }

    @java.lang.Override
    public void removeChildMember(Member member) {

    }

    @java.lang.Override
    public List<Member> getChildMembers() {

        return memberBawahan;
    }
    //TODO: Complete me
}
