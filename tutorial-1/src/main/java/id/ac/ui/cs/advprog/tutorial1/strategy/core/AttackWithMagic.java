package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
        //ToDo: Complete me
    public String getType(){
        return "Magic";
    }
    public String attack(){
        return "Attack with Magic";
    }
}
