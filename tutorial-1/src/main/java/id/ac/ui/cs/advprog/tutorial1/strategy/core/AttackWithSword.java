package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
        //ToDo: Complete me
    public String getType(){
        return "Sword";
    }
    public String attack(){
        return "Attack with Sword";
    }
}
