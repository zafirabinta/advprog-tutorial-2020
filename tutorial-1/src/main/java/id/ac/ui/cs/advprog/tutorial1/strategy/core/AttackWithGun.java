package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
        //ToDo: Complete me
    public String getType(){
        return "Gun";
    }
    public String attack(){
        return "Attack with Gun";
    }
}
