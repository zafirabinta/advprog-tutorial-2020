package id.ac.ui.cs.advprog.tutorial2.command.core.spell;
import java.util.*;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private ArrayList<Spell> arrayList;

    public ChainSpell(ArrayList<Spell> arrayList){
        this.arrayList = arrayList;
    }

    @java.lang.Override
    public void cast() {
        for (int i = 0; i < this.arrayList.size(); i++){
            this.arrayList.get(i).cast();
        }
    }

    @java.lang.Override
    public void undo() {
        for(int i = this.arrayList.size()-1; i >= 0; i--){
            this.arrayList.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
