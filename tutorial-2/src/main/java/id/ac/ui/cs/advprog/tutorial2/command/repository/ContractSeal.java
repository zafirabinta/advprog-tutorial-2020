package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    private Spell latestSpell;
    public ContractSeal() {
        spells = new HashMap<>();
    }
    private boolean undoFlag = false;

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        // TODO: Complete Me
        Spell thisSpell = spells.get(spellName);
        thisSpell.cast();
        latestSpell = thisSpell;
        undoFlag = false;
    }

    public void undoSpell() {
        // TODO: Complete Me
        if(this.undoFlag == false){
            latestSpell.undo();
            undoFlag = true;
        }
        else {}
        undoFlag = true;
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
